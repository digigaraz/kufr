import React from "react";
import { GridColumn, GridRow } from "semantic-ui-react";
import { PlayButton } from "../components/PlayButton/PlayButton";
import { I18n } from "../translations/I18n";

export class Intro extends React.PureComponent {
    render() {
        return (
            <GridRow>
                <GridColumn>
                    <h1>{I18n.t("Introduction")}</h1>
                </GridColumn>
                <GridColumn>
                    <PlayButton seconds={3} onTimeUp={() => alert("Game started")}>
                        Play
                    </PlayButton>
                </GridColumn>
            </GridRow>
        );
    }
}
