import { Field, FieldProps, Form, Formik, FormikActions, FormikErrors, FormikProps } from "formik";
import React from "react";
import { Button, Grid, Icon } from "semantic-ui-react";
import { Menu } from "../components/Menu/Menu";
import { MenuItem } from "../components/Menu/MenuItem";

interface IValues {
    email: string;
}

interface IUsersProps {
    setDimmed: (dim: boolean) => void;
}

export class Users extends React.PureComponent<IUsersProps> {
    render() {
        const { setDimmed } = this.props;
        return (
            <Grid container>
                <Grid.Row stretched columns="equal">
                    <Grid.Column width={4} doubling>
                        <Menu />
                    </Grid.Column>
                    <Grid.Column color="blue">
                        <header className="App-header">
                            {/*<img src={logo} className="App-logo" alt="logo" />*/}
                            <MenuItem value={10} onReachGoal={value => alert("Yea, je to tu " + value)}>
                                TEST - Ahoj světe
                            </MenuItem>

                            <Formik
                                initialValues={{
                                    email: "",
                                }}
                                onSubmit={(values: IValues, { setSubmitting }: FormikActions<IValues>) => {
                                    setTimeout(() => {
                                        alert(JSON.stringify(values, null, 2));
                                        setSubmitting(false);
                                    }, 500);
                                }}
                                validate={(values: IValues) => {
                                    let errors: FormikErrors<IValues> = {};
                                    if (!values.email) {
                                        errors.email = "Required";
                                    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
                                        errors.email = "Invalid email address";
                                    }
                                    return errors;
                                }}
                                render={(_: FormikProps<IValues>) => (
                                    <Form>
                                        <label htmlFor="firstName">First Name</label>
                                        <Field
                                            id="email"
                                            name="email"
                                            placeholder="john@acme.com"
                                            type="text"
                                            render={({ field, form }: FieldProps<IValues>) => (
                                                <div>
                                                    <input type="text" {...field} placeholder="First Name" />
                                                    {form.touched.email && form.errors.email && form.errors.email}
                                                </div>
                                            )}
                                        />
                                    </Form>
                                )}
                            />
                            <Button animated primary type="submit" onClick={() => setDimmed(true)}>
                                <Button.Content visible>Odeslat</Button.Content>
                                <Button.Content hidden>
                                    <Icon name="arrow right" />
                                </Button.Content>
                            </Button>
                        </header>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        );
    }
}
