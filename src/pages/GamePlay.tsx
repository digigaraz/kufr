import React from "react";
import { connect } from "react-redux";
import { Button, Icon } from "semantic-ui-react";
import styled from "styled-components";
import { GameContainer } from "../components/Container/Container";
import { BarColumn } from "../components/HeaderColumn/BarColumn";
// import { PlayButton } from "../components/PlayButton/PlayButton";
import { IScore, Score } from "../components/Score/Score";
import { Timer } from "../components/Timer/Timer";
import { Title } from "../components/Title/Title";
import { Word } from "../components/Word/Word";
import { IGameSettings } from "../store/game/types";
import { IReduxState } from "../store/store";
import { I18n } from "../translations/I18n";

export const GameCenter = styled("div")`
    flex: 1;
    display: flex;
    justify-content: stretch;
    align-items: center;
`;

export const GameHeader = styled("header")`
    display: flex;
    flex-direction: row;
    width: 100%;
    justify-content: space-between;
    align-items: stretch;
    padding: 0.5rem;
    padding: 0.5vw;
    background: #ffffff;
    color: #000;
`;

export const GameFooter = styled("footer")`
    display: flex;
    flex-direction: row;
    width: 100%;
    justify-content: center;
    align-items: stretch;
    padding: 0.5rem;
    padding: 1vh 0.5vw 3vh 0.5vw;
`;

export const WORDS = ["drn", "kamna", "překážka", "vzducholoď", "mrak", "bouřka"];
export const TEAM_NAMES = ["Červený", "Modrý", "Zelený", "Žlutý", "Fialový", "Hnědý", "Černý", "Oranžový"];

export interface IGamePlayProps {
    dictionary: string[];
    gameSettings: IGameSettings;
}

export interface IGamePlayStates {
    wordIndex: number;
    teamIndex: number;
    seconds: number;
    score: IScore;
}

export class GamePlay extends React.PureComponent<IGamePlayProps, IGamePlayStates> {
    constructor(props: IGamePlayProps, context: IGamePlayStates) {
        super(props, context);
        this.state = {
            wordIndex: 0,
            teamIndex: 0,
            seconds: props.gameSettings.roundSeconds + props.gameSettings.roundMinutes * 60,
            score: {
                correct: 0,
                skipped: 0,
                wrong: 0,
            },
        };
    }

    public changeWord = () => {
        const { dictionary } = this.props;
        this.setState(state => {
            const newIndex = ((state.wordIndex || 0) + 1) % dictionary.length;
            return {
                wordIndex: newIndex,
            };
        });
    };

    public formatSecondsToTime = (seconds: number) => {
        return ("0" + Math.floor(seconds / 60)).slice(-2) + ":" + ("0" + (seconds % 60)).slice(-2);
    };

    render() {
        const { wordIndex, teamIndex, score, seconds } = this.state;
        const { dictionary } = this.props;
        return (
            <GameContainer>
                <GameHeader>
                    <BarColumn left>
                        <Score score={score} />
                    </BarColumn>
                    <BarColumn>
                        <Title>
                            <Icon name="users" /> {TEAM_NAMES[teamIndex]}
                        </Title>
                    </BarColumn>
                    <BarColumn right>
                        <Timer>
                            <Icon name="time" /> {this.formatSecondsToTime(seconds)}
                        </Timer>
                    </BarColumn>
                </GameHeader>
                <GameCenter>
                    <Word>{dictionary && dictionary[wordIndex || 0]}</Word>
                </GameCenter>
                <GameFooter>
                    <BarColumn>
                        <Button circular size="huge" color="yellow">
                            <Icon name="step forward" />
                            {I18n.t("Skip")}
                        </Button>
                    </BarColumn>
                    <BarColumn>
                        <Button circular size="huge" color="green">
                            <Icon name="check" />
                            {I18n.t("Correct and continue")}
                        </Button>
                        {/*<PlayButton size="huge" seconds={3} onTimeUp={this.changeWord}>Play</PlayButton>*/}
                    </BarColumn>
                </GameFooter>
            </GameContainer>
        );
    }
}

const mapStateToProps = (store: IReduxState): IGamePlayProps => {
    console.log("WORDS", WORDS);
    return {
        gameSettings: store.game.settings!,
        dictionary: [...WORDS],
    };
};

const mapDispatchToProps = (_: any) => ({
    // setSettings: (settings: IGameSettings) => dispatch(setGameSettings(settings)),
});

export const GamePlayConnected = connect(
    mapStateToProps,
    mapDispatchToProps,
)(GamePlay);
