import { History } from "history";
import React, { FC } from "react";
import { connect } from "react-redux";
import { RouterProps } from "react-router";
import { Button, Icon } from "semantic-ui-react";
import { CenteredContainer, Container } from "../components/Container/Container";
import { Label } from "../components/Label/Label";
import { NumberInput } from "../components/NumberInput/NumberInput";
import { SettingsItem } from "../components/SettingsItem/SettingsItem";
import { setGameSettings } from "../store/game/actions";
import { IGameSettings } from "../store/game/types";
import { I18n } from "../translations/I18n";

const TEAM_SIZE_INPUT = "team_size_input";
const ROUND_TIME_INPUT = "round_time_input";

export interface ISettingsProps {}
export interface ISettingsStateProps {
    gameSettings: IGameSettings;
}
export interface ISettingsDispatchProps {
    setSettings: (settings: IGameSettings) => void;
}
export type SettingsProps = ISettingsProps & ISettingsStateProps & ISettingsDispatchProps & RouterProps;

const goToScreen = (history: History) => {
    history.push("/play");
};

export const Settings: FC<SettingsProps> = ({ history, setSettings, gameSettings }: SettingsProps) => {
    const onContinue = () => {
        if (gameSettings.roundMinutes > 0 || gameSettings.roundSeconds > 0) {
            goToScreen(history);
        }
    };

    return (
        <CenteredContainer>
            <Container>
                <h1>{I18n.t("Game settings")}</h1>
                <SettingsItem labelText={I18n.t("Team count")} labelTarget={TEAM_SIZE_INPUT}>
                    <NumberInput
                        min={2}
                        max={20}
                        value={gameSettings.teamCount}
                        inputId={TEAM_SIZE_INPUT}
                        buttons
                        onChange={value => {
                            setSettings({
                                ...gameSettings,
                                teamCount: value,
                            });
                        }}
                    />
                </SettingsItem>
                <SettingsItem labelText={I18n.t("Round time")}>
                    <Label htmlFor={ROUND_TIME_INPUT}>{I18n.t("Minutes")}</Label>
                    <NumberInput
                        min={0}
                        max={10}
                        value={gameSettings.roundMinutes}
                        step={1}
                        buttons
                        onChange={value => {
                            setSettings({
                                ...gameSettings,
                                roundMinutes: value,
                            });
                        }}
                    />
                    <Label>{I18n.t("Seconds")}</Label>
                    <NumberInput
                        min={0}
                        max={45}
                        value={gameSettings.roundSeconds}
                        step={15}
                        buttons
                        onChange={value => {
                            setSettings({
                                ...gameSettings,
                                roundSeconds: value,
                            });
                        }}
                    />
                </SettingsItem>
                <SettingsItem>
                    <Button fluid color="green" size="big" circular onClick={onContinue}>
                        <Icon name="play" /> {I18n.t("Continue")}
                    </Button>
                </SettingsItem>
            </Container>
        </CenteredContainer>
    );
};

const mapStateToProps = (store: any): ISettingsStateProps => {
    console.log("mapStateToProps", store);
    return {
        gameSettings: store.game.settings!,
    };
};

const mapDispatchToProps = (dispatch: any) => ({
    setSettings: (settings: IGameSettings) => dispatch(setGameSettings(settings)),
});

export const SettingsConnected = connect<ISettingsStateProps, ISettingsDispatchProps, ISettingsProps>(
    mapStateToProps,
    mapDispatchToProps,
)(Settings);
