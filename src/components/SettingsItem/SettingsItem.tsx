import React, { FC, ReactNode } from "react";
import styled from "styled-components";
import { Label } from "../Label/Label";

export interface ISettingsItemProps {
    labelText?: string;
    title?: string;
    children?: ReactNode;
    labelTarget?: string;
}

const StyledWrap = styled("div")`
    margin: 0 auto 1rem;
    width: 100%;
`;

export const SettingsItem: FC<ISettingsItemProps> = ({ labelText, children, labelTarget }: ISettingsItemProps) => {
    return (
        <StyledWrap>
            {labelText !== undefined && <Label strong htmlFor={labelTarget}>{labelText}</Label>}
            {children}
        </StyledWrap>
    );
};
