import React, { FC, ReactNode } from "react";
import styled from "styled-components";

export interface ILabelProps extends React.LabelHTMLAttributes<HTMLLabelElement> {
    children?: ReactNode;
    strong?: boolean;
}

const StyledLabel = styled("label")`
    font-size: 1em;
    line-height: 1.5em;
    text-align: center;
    display: block;
    margin: 0.5em;
    text-transform: uppercase;
    font-weight: ${(props:ILabelProps) => (props.strong ? "900" : "400")};
`;

export const Label: FC<ILabelProps> = ({ children, ...props }: ILabelProps) => {
    return <StyledLabel {...props}>{children}</StyledLabel>;
};
