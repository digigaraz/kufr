import React from "react";
import { Button } from "semantic-ui-react";
import { StyledInput, ButtonRight, InputWrap, StyledWrap } from "./NumberInput.styles";

export interface INumberInputProps {
    value?: number;
    max?: number;
    min?: number;
    step?: number;
    buttons?: boolean;
    icon?: string;
    label?: string;
    inputId?: string;
    onChange?: (value: number) => void;
}

export interface INumberInputState {
    value: number;
}

const getInitialValue = (props: INumberInputProps) => (props.value !== undefined ? props.value : 1);

export class NumberInput extends React.PureComponent<INumberInputProps, INumberInputState> {
    readonly state = { value: getInitialValue(this.props) };

    private parseNumber = (value: number): number => {
        const { max, min } = this.props;
        const parsedValue = Number.parseInt(value + "");
        let result = parsedValue;
        if (!isNaN(parsedValue)) {
            if (min !== undefined && value < min) {
                result = min;
            }
            if (max !== undefined && value > max) {
                result = max;
            }
        }

        return result;
    };

    private canSet = (value: number) => {
        return !isNaN(Number.parseInt(value + ""));
    };

    private addValue = (diff: number) => {
        const value = this.state.value;
        this.setValue(value + diff);
    };

    private setValue = (rawValue: number) => {
        const { onChange } = this.props;
        if (this.canSet(rawValue)) {
            const value = this.parseNumber(rawValue);
            this.setState({ value });
            onChange !== undefined && onChange(value);
        }
    };

    private increment = () => {
        this.addValue(this.props.step || 1);
    };

    private decrement = () => {
        this.addValue(-(this.props.step || 1));
    };

    public render = () => {
        const { value } = this.state;
        const { buttons, icon, inputId, label } = this.props;
        return (
            <StyledWrap>
                <div>{buttons && <Button type="button" circular color="yellow" icon="minus" onClick={this.decrement} />}</div>
                <InputWrap>
                    <StyledInput
                        icon={icon}
                        id={inputId}
                        value={value}
                        label={label !== undefined && label}
                        labelPosition={label !== undefined ? "right" : undefined}
                        onChange={(e: React.FormEvent<HTMLInputElement>) => {
                            const number = Number.parseInt(e.currentTarget.value);
                            if (!isNaN(number)) {
                                this.setValue(number);
                            }
                        }}
                    />
                </InputWrap>
                {buttons && <ButtonRight type="button" circular color="yellow" icon="plus" onClick={this.increment} />}
            </StyledWrap>
        );
    };
}
