import { Button, Input } from "semantic-ui-react";
import styled from "styled-components";

export const StyledInput = styled(Input)`
    &.ui.input {
        font-size: 1.2em;
        flex: 1 0 auto;
        width: 100%;
        max-width: 100%;
        input {
            width: 100%;
            font-size: 1em;
            text-align: center;
            flex: 1;
        }
        .label {
            min-width: 2.5em;
            text-align: center;
        }
    }
`;

export const StyledWrap = styled("div")`
    font-size: 1rem;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
`;

export const InputWrap = styled("div")`
    flex: 1;
`;

export const ButtonRight = styled(Button)`
    &.ui.button {
        margin: 0 0 0 0.25em;
    }
`;
