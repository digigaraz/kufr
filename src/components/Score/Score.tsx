import React, { FC } from "react";
import { Icon } from "semantic-ui-react";
import styled from "styled-components";

export interface IScore {
    correct: number;
    wrong: number;
    skipped: number;
}

export interface IScoreProps extends React.LabelHTMLAttributes<HTMLDivElement> {
    score: IScore;
}

const Wrap = styled("div")`
    font-size: 2rem;
    font-weight: 400;
    line-height: 1.5em;
    text-align: center;
    display: block;
    color: #000;
    text-shadow: 2px 2px 0 rgba(255, 255, 255, 0.5);
`;

export const Score: FC<IScoreProps> = ({ children, score, ...props }: IScoreProps) => {
    return (
        <Wrap {...props}>
            <Icon name="check"/> {score.correct} /
            <Icon name="times"/> {score.skipped}
        </Wrap>
    );
};
