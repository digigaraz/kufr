import React, { FC, ReactNode, useEffect, useRef, useState } from "react";
import styled from "styled-components";

export interface IWordProps extends React.LabelHTMLAttributes<HTMLHeadingElement> {
    children?: ReactNode;
    strong?: boolean;
    maxFontSize?: number;
    minFontSize?: number;
}

const StyledWord = styled("div")`
    font-size: 10rem;
    font-weight: 900;
    line-height: 1em;
    text-align: center;
    display: block;
    text-transform: uppercase;
    border-radius: 0.1em;
    padding: 0 0.2em;
    color: #ffffff;
    text-shadow: 2px 2px 0 rgba(0,0, 0, 0.5);
    //background-image: linear-gradient(to bottom right, #00ff78 , #029dff);
`;

export const Word: FC<IWordProps> = ({ children, maxFontSize = 15, minFontSize = 2, ...props }: IWordProps) => {
    const [windowWidth, setWindowWidth] = useState<number>(window.innerWidth);
    const [size, setSize] = useState<number>(10);

    const onResize = () => {
        setWindowWidth(window.innerWidth);
    };

    useEffect(() => {
        window.addEventListener("resize", onResize);
        return () => {
            window.removeEventListener("mousemove", onResize);
        };
    }, []);

    const ref = useRef<HTMLDivElement>(null);
    useEffect(
        () => {
            if (ref.current !== null) {
                const current = ref.current!;
                const actualSize = size;
                const textWidth = current.getBoundingClientRect().width;
                const newSize = Math.max(
                    minFontSize,
                    Math.min(maxFontSize, ((actualSize * (windowWidth - 20)) / textWidth)),
                );
                console.log(actualSize, newSize, maxFontSize, textWidth, windowWidth);
                setSize(newSize);
            }
        },
        [windowWidth, children],
    );
    return (
        <StyledWord ref={ref} style={{ fontSize: size + "rem" }} {...props}>
            {children}
        </StyledWord>
    );
};
