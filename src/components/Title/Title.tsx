import React, { FC } from "react";
import styled from "styled-components";

export interface ITitleProps extends React.LabelHTMLAttributes<HTMLDivElement> {
    strong?: boolean;
}

const StyledTitle = styled("div")`
    font-size: 2rem;
    font-weight: ${(props:ITitleProps) => props.strong ? 900 : 400};
    line-height: 1.5em;
    text-align: center;
    display: block;
    color: #000;
    text-shadow: 2px 2px 0 rgba(255, 255, 255, 0.5);
`;

export const Title: FC<ITitleProps> = ({ children, ...props }: ITitleProps) => {
    return <StyledTitle {...props}>{children}</StyledTitle>;
};
