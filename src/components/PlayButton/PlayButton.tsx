import React from "react";
import { Button, ButtonProps, Icon } from "semantic-ui-react";
import { I18n } from "../../translations/I18n";

export interface IPlayButtonProps extends ButtonProps{
    seconds?: number;
    onTimeUp: () => void;
}

export interface IPlayButtonState {
    seconds: number;
    started: boolean;
    timerId?: number;
}

const DEFAULT_SECONDS = 5;

export class PlayButton extends React.PureComponent<IPlayButtonProps, IPlayButtonState> {
    constructor(props: IPlayButtonProps, state: IPlayButtonState) {
        super(props, state);
        this.state = {
            started: false,
            seconds: props.seconds || DEFAULT_SECONDS,
        };
    }

    private decrement = () => {
        const { started, seconds, timerId } = this.state;
        if (started && timerId !== undefined) {
            if (seconds > 0) {
                this.setState(
                    state => ({ seconds: state.seconds - 1 }),
                    () => {
                        if (this.state.seconds === 0) {
                            this.clearTimer();
                            this.props.onTimeUp();
                        }
                    },
                );
            }
        }
    };

    private clearTimer = () => {
        clearInterval(this.state.timerId);
        this.setState({
            timerId: undefined,
            started: false,
            seconds: this.props.seconds || DEFAULT_SECONDS,
        });
    };

    private startTimer = () => {
        if (this.state.timerId === undefined) {
            const timerId = setInterval(this.decrement, 1000);
            this.setState({
                seconds: this.props.seconds || DEFAULT_SECONDS,
                started: true,
                timerId,
            });
        } else {
            this.clearTimer();
        }
    };

    public render = () => {
        const { started, seconds } = this.state;
        const { seconds: defaultSeconds, onTimeUp, ...props } = this.props;

        return (
            <Button
                circular
                color="green"
                onClick={this.startTimer}
                {...props}
                title={started ? I18n.t("Cancel") : I18n.t("Start")}
            >
                {started ? <Icon name="stop" /> : <Icon name="play" />} {seconds} s
            </Button>
        );
    };
}
