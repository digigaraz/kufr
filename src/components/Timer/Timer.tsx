import React, { FC } from "react";
import styled from "styled-components";
import { Title } from "../Title/Title";

export interface ITimerProps extends React.LabelHTMLAttributes<HTMLDivElement> {
    onTimeUp?:()=>void;
    seconds?: number;
}

const StyledTimer = styled(Title)`
    font-weight: 900;
`;

export const Timer: FC<ITimerProps> = ({ children, ...props }: ITimerProps) => {
    return <StyledTimer {...props}>{children}</StyledTimer>;
};
