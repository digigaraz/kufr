import React, { FC, ReactNode, useEffect, useState } from "react";
import { Button } from "semantic-ui-react";

export interface IMenuItemProps {
    value?: number;
    children?: ReactNode;
    onReachGoal?: (value: number) => void;
}

export const MenuItem: FC<IMenuItemProps> = ({ value = 0, children, onReachGoal }: IMenuItemProps) => {
    const [count, setCount] = useState<number>(value);
    const increment = () => {
        setCount(count + 1);
    };

    useEffect(
        () => {
            if (count >= 12) {
                onReachGoal !== undefined && onReachGoal(count);
                setCount(0);
            }
        },
        [count, onReachGoal],
    );

    return (
        <Button primary onClick={increment}>
            {children} {count}
        </Button>
    );
};
