import React, { FC, ReactNode, useState } from "react";
import { Dropdown, Icon, Input, Menu as UiMenu } from "semantic-ui-react";
import { MenuItem } from "./MenuItem";

export interface IMenuProps {
    value?: number;
    children?: ReactNode;
    onReachGoal?: (value: number) => void;
}

export const Menu: FC<IMenuProps> = () => {
    const [activeItem, setActiveItem] = useState("home");
    const handleItemClick = (_: any, { name }: any) => setActiveItem(name);

    return (
        <UiMenu vertical fluid>
            <UiMenu.Item>
                <Input placeholder="Search..." />
            </UiMenu.Item>
            <UiMenu.Item>
                Home
                <UiMenu.Menu>
                    <UiMenu.Item name="main_menu" active={activeItem === "main_menu"} onClick={handleItemClick}>
                        <MenuItem onReachGoal={() => alert("ajeto")}>Hlavní nabídka</MenuItem>
                    </UiMenu.Item>
                    <UiMenu.Item name="add" active={activeItem === "add"} onClick={handleItemClick}>
                        Add
                    </UiMenu.Item>
                    <UiMenu.Item name="about" active={activeItem === "about"} onClick={handleItemClick}>
                        Remove
                    </UiMenu.Item>
                </UiMenu.Menu>
            </UiMenu.Item>
            <UiMenu.Item name="browse" active={activeItem === "browse"} onClick={handleItemClick}>
                <Icon name="grid layout" />
                Browse
            </UiMenu.Item>
            <UiMenu.Item name="messages" active={activeItem === "messages"} onClick={handleItemClick}>
                Messages
            </UiMenu.Item>
            <Dropdown item text="More">
                <Dropdown.Menu>
                    <Dropdown.Item icon="edit" text="Edit Profile" />
                    <Dropdown.Item icon="globe" text="Choose Language" />
                    <Dropdown.Item icon="settings" text="Account Settings" />
                </Dropdown.Menu>
            </Dropdown>
        </UiMenu>
    );
};
