import React, { FC } from "react";
import styled from "styled-components";

export interface IBarColumnProps extends React.LabelHTMLAttributes<HTMLDivElement> {
    left?: boolean;
    right?: boolean;
}

export const StyledBarColumn = styled("div")`
    flex: 1;
    display: flex;
    text-align: center;
    justify-content: ${(props: IBarColumnProps) =>
        props.left ? "flex-start" : props.right ? "flex-end" : "center"};
`;

export const BarColumn: FC<IBarColumnProps> = ({ children, ...props }: IBarColumnProps) => {
    return <StyledBarColumn {...props}>{children}</StyledBarColumn>;
};
