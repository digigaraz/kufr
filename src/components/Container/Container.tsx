import styled from "styled-components";

export const Container = styled("div")`
    max-width: 300px;
    max-height: 500px;
    min-height: 500px;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
`;

export const CenteredContainer = styled("div")`
    max-width: 100%;
    min-height: 100vh;
    width: 100%;
    text-align: center;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    color: #ffffff;
    background-color: #005fbd;
    background-image: linear-gradient(to bottom right, #009e6f, #005fbd);
`;

export const GameContainer = styled("div")`
    max-width: 100%;
    min-height: 100vh;
    width: 100%;
    text-align: center;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
    color: #ffffff;
    background-color: #005fbd;
    background-image: linear-gradient(to bottom right, #009e6f, #005fbd);
`;
