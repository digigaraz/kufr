export interface IGameState {
    settings?: IGameSettings;
}

export interface IGameSettings {
    teamCount: number;
    roundMinutes: number;
    roundSeconds: number;
}

export const SET_SETTINGS = "SET_SETTINGS";

interface setGameSettingsAction {
    type: typeof SET_SETTINGS;
    payload: IGameSettings;
}

export type GameActionTypes = setGameSettingsAction;
