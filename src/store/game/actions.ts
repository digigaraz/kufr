import { GameActionTypes, IGameSettings, SET_SETTINGS } from "./types";

export function setGameSettings(settings: IGameSettings): GameActionTypes {
    console.log("settings", settings);
    return { type: SET_SETTINGS, payload: settings };
}
