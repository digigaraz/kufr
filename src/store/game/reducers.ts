import { Reducer } from "redux";
import { GameActionTypes, IGameState } from "./types";

const initialState: IGameState = {
    settings: {
        teamCount: 3,
        roundMinutes: 2,
        roundSeconds: 0,
    },
};

export const gameReducers: Reducer<IGameState, GameActionTypes> = (
    state: IGameState = initialState,
    action: GameActionTypes,
): IGameState => {
    switch (action.type) {
        case "SET_SETTINGS":
            return {
                ...state,
                settings: { ...action.payload },
            };
        default:
            return state;
    }
};
