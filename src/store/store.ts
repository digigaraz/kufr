import { combineReducers } from "redux";
import { gameReducers } from './game/reducers';
import { IGameState } from "./game/types";

export interface IReduxState {
    game: IGameState;
}

export const rootReducer = combineReducers({
    game: gameReducers,
});

export type AppState = ReturnType<typeof rootReducer>