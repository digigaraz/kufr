import React, { useState } from "react";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { createStore } from "redux";
import { Dimmer, Loader } from "semantic-ui-react";
import { GameOver } from "./pages/GameOver";
import { GamePlayConnected as GamePlay } from "./pages/GamePlay";
import { Intro } from "./pages/Intro";
import { SettingsConnected as Settings } from "./pages/Settings";
import { Statistics } from "./pages/Statistics";
import { rootReducer } from "./store/store";

// import logo from "./logo.svg";

export const store = createStore(
    rootReducer
    // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export const App: React.FC = () => {
    const [isDimmed, setDimmed] = useState(false);
    return (
        <Provider store={store}>
            <div className="App">
                <Router>
                    <Switch>
                        <Route exact path="/" component={Intro} />
                        <Route path="/settings" component={Settings} />
                        <Route path="/play" component={GamePlay} />
                        <Route path="/gameOver" component={GameOver} />
                        <Route path="/statistics" component={Statistics} />
                    </Switch>
                </Router>

                <Dimmer active={isDimmed} onClickOutside={() => setDimmed(false)} page inverted>
                    <Loader>Loading</Loader>
                </Dimmer>
            </div>
        </Provider>
    );
};

export default App;
