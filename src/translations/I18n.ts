import i18next from "i18next";
import LanguageDetector from "i18next-browser-languagedetector";

import cs from "../public/locales/cs/translations.json";
import en from "../public/locales/en/translations.json";

i18next.use(LanguageDetector).init({
    // we init with resources
    resources: {
        en: {
            translations: en,
        },
        cs: {
            translations: cs,
        },
    },
    fallbackLng: "cs",
    debug: true,

    // have a common namespace used around the full app
    ns: ["translations"],
    defaultNS: "translations",

    keySeparator: false, // we use content as keys

    interpolation: {
        escapeValue: false, // not needed for react!!
        formatSeparator: ",",
    },

    react: {
        wait: true,
    },
});

export const I18n = i18next;
